# README #

Ahoi,

cool, dass du hier gelandet bist. User Apps für Knuddels MyChannel zu schreiben macht viel Spaß!


Im [Projekt-Wiki](https://bitbucket.org/knuddels/user-apps/wiki) findest du viele wichtige Informationen dazu, wie du am besten startest.

### Wofür ist dieses Repository? ###

* Beispiel-Code für User Apps
* Lernen, wie man User Apps baut


### Wie kann ich mitwirken? ###

* Werde Entwickler (im Chat mit /apps developer) und schreibe eigene User Apps für Knuddels
* Verbessere [unsere Beispiel-Apps](https://bitbucket.org/knuddels/user-apps/src) und erstelle [Pull-Requests](https://bitbucket.org/knuddels/user-apps/pull-requests)
* Aktualisiere das [Wiki](https://bitbucket.org/knuddels/user-apps/wiki/Home)
* Viele weitere Tutorials, Tipps und Informationen rund um User Apps, findest du im [Blog](https://blog.knuddels.de)