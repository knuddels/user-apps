/*
Hey there. 
Sieh mal an wie weit du schon gekommen bist. Du weißt jetzt wie du Informationen aus einem User Objekt auslesen kannst und wie du diese verarbeitest.
Jetzt werden wir uns um den Transfer von Knuddels kümmern.
Und los gehts.

*/

var App = {};

// Wir definieren ein neues Objekt usersPaid. In ein Objekt kannst du mehrere Objekte legen und auch wieder von dort herrausnehmen. 
var usersPaid = {};

// Wir definieren eine neue Variable, die den Namen des Appbots enthalten soll.
var appBotName = KnuddelsServer.getDefaultBotUser().getNick();

App.onUserJoined = function(user)
{		
	var gender = user.getGender();
	var age = user.getAge();	
	var nick = user.getNick();
	var genderGreeting;
	
	if (gender == Gender.Male)
	{
		genderGreeting = 'Prinz ' + nick;
	}
	else if (gender == Gender.Female)
	{
		genderGreeting = 'Prinzessin ' + nick;
	}
	else
	{
		genderGreeting = nick;
	}
	
	/* Wir wollen es dem neuen User natürlich so einfach wie möglich machen deinem App Bot eine Knuddel zu transferieren. Lass uns dazu den von Knuddels aktzeptierten KCode verwenden und einen Link bauen. 
	 Merke: Links kann wirklich nur ein ChannelBot verwenden.
	 Also was steht da eigentlich? Los geht das ganze mit _ und hört mit _ auf. Alles dazwischen wird fett gedruckt. Da unser Link auch noch eine spezielle Farbe haben soll, setzen wir hinter den _ ein °BB° was nichts anderes
	 als default Channelblau bedeutet. In den meisten Fällen wird dein Text dadurch irgend eine Art Blau annehmen. Was genau die default Channelblau Farbe aber ist, bestimmt immer der Channeleigentümer. Also auch du für deinen
	 eigenen Channel. Geh dazu über /mychannel in die Grundeinstellungen deines Channels und klicke dann auf Detaildesigneinstellungen Erweitert. 
	 Zum Schluss kommt der Link. Alles zwischen °> und <° wird in Knuddels als Link dargestellt. Dein Link brauch immer ein "Ziel" und einen Text der dargestellt werden soll. 
	 Diese trennst du durch | , bei uns ist das also °>einen Knuddel | /appknuddel + der Name deines Appbots <° 
	 Zwei kleine Details fehlen uns jetzt noch. Hinter °> folgt _h, was einen hover Link beschreibt. Ein Link also der erst dann unterstrichen ist, wenn du mit deiner Maus darüberfährst. 
	 appBotName.escapeKCode() verwenden wir, da unser KCode ohne .escapeKCode() nicht funktionieren würde, falls dein Appbot einen Namen hat, der Zeichen wie < oder > etc. enthält. 
	 Wenn du mehr dazu lesen willst empfehle ich dir folgende Seite in unserem Wiki: https://bitbucket.org/knuddels/user-apps/wiki/browse/KCode
	*/
	var message = 'Hey ' + genderGreeting +  ', willkommen im Channel. Du bist ' + age + ' Jahre alt. Bist du so nett und gibst mir _°BB°°>_heinen Knuddel|/appknuddel ' + appBotName.escapeKCode() + '<°°°_?';
	user.sendPrivateMessage(message);
	
	// Jetzt warten wir 30 Sekunden. Dann überprüfen wir, ob wir Knuddels erhalten haben.
	// Die Funktion setTimeout stoppt den aktuellen Ablauf des Skriptes und fährt erst nach der gesetzten Zeit (hier 30000 Millisekunden) wieder fort. 
	setTimeout(function() {
		checkIfKnuddelReceived(user);
	}, 30000); 		
};

// Da wir natürlich wissen wollen ob der User uns Knuddel transferiert müssen wir die Funktion onKnuddelReceived implementieren. Diese wird immer dann ausgeführt,
// wenn ein User deinem Appbot Knuddel transferiert hat.
App.onKnuddelReceived = function(sender, receiver, knuddelAmount, transferReason)
{
	// Der User hat innerhalb 30 Sekunden einen oder mehrere Knuddel an den Appbot transferiert. Wir antworten also mit einem Dankeschön.
	sender.sendPrivateMessage('Dankeschön ' + sender.getNick() + '!');
	
	// Wir fügen den User der Knuddel überwießen hat zum Objekt usersPaid hinzu.
	// Was hier genau passiert ist folgendes: Der Nickname des Users wird mit einem key (Schlüssel, der Schlüssel ist hier die UserId des Users), 
	// in das Objekt usersPaid eingefügt. Der Key ist sehr wichtig, da jedes Objekt durch ihn wieder eindeutig identifiziert werden kann.
	usersPaid[sender.getUserId()] = sender.getNick();
};

// Hier definieren wir die Funktion, die überprüft ob innerhalb der 30 Sekunden Knuddels empfangen wurden oder nicht.
function checkIfKnuddelReceived(user)
{
	if (!usersPaid[user.getUserId()])
	{
		// Der User hat noch keine Knuddel an den Appbot transferiert. Wir antworten traurig. 
		user.sendPrivateMessage('Schade... ich hätte so gerne einen Knuddel gehabt ' + user.getNick() + '.');	
	}
}